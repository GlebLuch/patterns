package adapter_lab;

import adapter_lab.adapter.Adapter;
import adapter_lab.round.RoundHole;
import adapter_lab.round.RoundPeg;
import adapter_lab.square.SquarePeg;

public class Main {
    public static void main(String[] args) {
        RoundHole hole = new RoundHole(20);
        RoundPeg roundPeg = new RoundPeg(10);
        if (hole.fits(roundPeg)) {
            System.out.println("Круглый колышек с радиусом " + roundPeg.getRadius() +
                    " встает в круглое отверстие с радиусом " + hole.getRadius());
        }

        SquarePeg smallSquarePeg = new SquarePeg(5);
        SquarePeg largeSquarePeg = new SquarePeg(30);

        Adapter smallSquarePegAdapter = new Adapter(smallSquarePeg);
        Adapter largeSquarePegAdapter = new Adapter(largeSquarePeg);
        if (hole.fits(smallSquarePegAdapter)) {
            System.out.println("Квадратный колышек с шириной " + smallSquarePeg.getWidth() +
                    " встает в круглое отверстие с радиусом " + hole.getRadius());
        }
        if (!hole.fits(largeSquarePegAdapter)) {
            System.out.println("Квадратный колышек с шириной " + largeSquarePeg.getWidth() +
                    " не встает в круглое отверстие с радиусом " + hole.getRadius());
        }
    }
}

