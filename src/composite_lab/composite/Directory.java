package composite_lab.composite;

import java.util.ArrayList;
import java.util.List;

public class Directory extends Component {

    private List<Component> components = new ArrayList<Component>();

    public Directory(String name) {
        super(name);
    }

    @Override
    public void add(Component component) {
        components.add(component);
    }

    @Override
    public void remove(Component component) {
        components.remove(component);
    }

    @Override
    public void print() {
        System.out.println("Узел " + name);
        System.out.println("Подузлы:");
        for (int i = 0; i < components.size(); i++) {
            components.get(i).print();
        }
    }
}
