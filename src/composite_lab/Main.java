package composite_lab;

import composite_lab.composite.Component;
import composite_lab.composite.Directory;
import composite_lab.composite.File;

public class Main {
    public static void main(String[] args) {

        Component fileSystem = new Directory("Файловая система");
        Component diskC = new Directory("Диск С");
        Component pngFile = new File("12345.png");
        Component docxFile = new File("Document.docx");
        diskC.add(pngFile);
        diskC.add(docxFile);
        fileSystem.add(diskC);
        fileSystem.print();
        System.out.println();
        diskC.remove(pngFile);
        Component docsFolder = new Directory("Мои Документы");
        Component txtFile = new File("readme.txt");
        Component csFile = new File("Program.cs");
        docsFolder.add(txtFile);
        docsFolder.add(csFile);
        diskC.add(docsFolder);

        fileSystem.print();
    }
}
