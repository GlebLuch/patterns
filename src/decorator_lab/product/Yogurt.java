package decorator_lab.product;

public class Yogurt implements Product{
    @Override
    public String Sale() {
        return "Продается йогурт";
    }
}
