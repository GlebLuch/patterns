package decorator_lab.product;

public class IceCream implements Product{
    @Override
    public String Sale() {
        return "Продается мороженное";
    }
}
