package decorator_lab.decorator;

import decorator_lab.product.Product;

public class Sprinkle implements Product {

    public  final Product product;

    public Sprinkle(Product product) {
        this.product = product;
    }

    @Override
    public String Sale() {
        return product.Sale() + " с посыпкой";
    }
}
