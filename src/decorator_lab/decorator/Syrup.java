package decorator_lab.decorator;

import decorator_lab.product.Product;

public class Syrup implements Product {

    private final Product product;

    public Syrup(Product product) {
        this.product = product;
    }

    @Override
    public String Sale() {
        return product.Sale() + " с сиропом";
    }
}
