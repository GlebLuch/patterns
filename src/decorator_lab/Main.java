package decorator_lab;

import decorator_lab.decorator.Syrup;
import decorator_lab.product.IceCream;
import decorator_lab.product.Smoothie;
import decorator_lab.product.Yogurt;

public class Main {
    public static void main(String[] args) {

        IceCream iceCream = new IceCream();
        Yogurt yogurt = new Yogurt();
        Smoothie smoothie = new Smoothie();

        System.out.println(iceCream.Sale());
        System.out.println(yogurt.Sale());
        System.out.println(smoothie.Sale());

        Syrup iceCreamWithSyrup = new Syrup(iceCream);
        System.out.println(iceCreamWithSyrup.Sale());
    }
}
