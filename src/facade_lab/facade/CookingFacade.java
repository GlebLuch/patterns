package facade_lab.facade;

import facade_lab.cook.Dough;
import facade_lab.cook.PanCake;
import facade_lab.cook.Ware;

public class CookingFacade {

    public void cookingPanCakes(int count) {

        Dough dough = new Dough();
        Ware ware = new Ware();
        PanCake panCake = new PanCake();

        dough.make();
        ware.prepare();

        for (int i = 1; i<=count; i++) {
            panCake.cook();
        }
    }
}
