package facade_lab;

import facade_lab.facade.CookingFacade;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Сколько приготовить блинов? ");
        var count = scanner.nextInt();
        CookingFacade cookingFacade = new CookingFacade();
        cookingFacade.cookingPanCakes(count);
    }
}
